%% Ground Truth for real
% [beam_1_bot, beam_1_top, beam_2_bot, beam_2_top, beam_3_bot, beam_3_top, beam_4_bot, beam_4_top];
ground_truth = zeros(3,8);

beam_height = 18.0;

ground_truth(:,1) = [0; 0; 0];
ground_truth(:,2) = [0; 0; beam_height];

ground_truth(:,3) = [2.46; 0; 0];
ground_truth(:,4) = [2.46; 0; beam_height];

ground_truth(:,5) = [2.459; 12.238; 0];
ground_truth(:,6) = [2.459; 12.238; beam_height];

ground_truth(:,7) = [0; 12.236; 0];
ground_truth(:,8) = [0; 12.236; beam_height];

for k = 1:4
   X = [ground_truth(1,2*k-1:2*k)]; 
   Y = [ground_truth(2,2*k-1:2*k)]; 
   Z = [ground_truth(3,2*k-1:2*k)]; 
   line(X, Y, Z, 'linewidth', 3, 'color', [0.7 0.7 0.7])
end
