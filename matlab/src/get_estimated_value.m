function [theta_1, theta_2, depth, height] = get_estimated_value(data)

theta_1 = data(:,5);
theta_2 = data(:,6);
depth = data(:,7)*1000;
height = data(:,8)*1000+518.25;


end

