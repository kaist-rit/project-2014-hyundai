clear all;
close all;


addpath('../gtsam_toolbox');
import gtsam.*

%% RANSAC?�로 ?��? 코너 ?�보�?로드?�다.
load('corners_case1.mat');
%load('corners.mat');

noise_model_for_init = [5*pi/180; 5*pi/180; 1*pi/180; 0.5; 0.5; 0.5];
noise_model_for_step = [0.1*pi/180; 0.1*pi/180; 0.1*pi/180; 0.01; 0.01; 0.01];
noise_model_for_between = [0.1*pi/180; 0.1*pi/180; 0.1*pi/180; 0.03; 0.03; 0.01];
noise_model_for_sensor = [1*pi/180; 1*pi/180; 5*pi/180; 0.01; 0.01; 0.01];

%% ?�이�?모델 ?�의  [!!!!! 중요 !!!!]
noise_model_for_init_robot_pose = noiseModel.Diagonal.Sigmas(noise_model_for_init);
noise_model_for_step_robot_pose = noiseModel.Diagonal.Sigmas(noise_model_for_step);     
noise_model_for_corner_to_corner =  noiseModel.Diagonal.Sigmas(0.1*noise_model_for_between);    % For mID = 1  
noise_model_for_step_corner = noise_model_for_step_robot_pose;

noise_model_for_robot_to_corner = cell(6,1);                                           
noise_model_for_robot_to_corner{1} = noiseModel.Diagonal.Sigmas(1.0*noise_model_for_sensor);    % For mID = 1   
noise_model_for_robot_to_corner{2} = noiseModel.Diagonal.Sigmas(0.5*noise_model_for_sensor);    % For mID = 1   
noise_model_for_robot_to_corner{3} = noiseModel.Diagonal.Sigmas(1.5*noise_model_for_sensor);    % For mID = 1   
noise_model_for_robot_to_corner{4} = noiseModel.Diagonal.Sigmas(1.0*noise_model_for_sensor);    % For mID = 1   
noise_model_for_robot_to_corner{5} = noiseModel.Diagonal.Sigmas(1.5*noise_model_for_sensor);    % For mID = 1   
noise_model_for_robot_to_corner{6} = noiseModel.Diagonal.Sigmas(0.5*noise_model_for_sensor);    % For mID = 1   


noise_model_for_corner_to_robot = cell(6,1);                                           % 같�? 빔에??측정??코너??�?빔을 ??��?�는 로봇 ?�이???�이�?모델
noise_model_for_corner_to_robot{1} = noiseModel.Diagonal.Sigmas(1.5*noise_model_for_between);  
noise_model_for_corner_to_robot{2} = noiseModel.Diagonal.Sigmas(0.5*noise_model_for_between); 
noise_model_for_corner_to_robot{3} = noiseModel.Diagonal.Sigmas(1.0*noise_model_for_between);



%% 로봇 ?�드�??�의?�다. (�?Measurement ID???�라 초기 x,y 값을 결정)
robot_pose = cell(3,1);
for ii = 1:measurementID/2
    switch ii
        case 1
            robot_pose{ii} = repmat(eye(4,4), 1, scan_num);           
            for k = 1:scan_num
               robot_pose{ii}(1:2,(k-1)*4+1:(k-1)*4+2)  = [cos(0.0*pi/180) -sin(0.0*pi/180); sin(0.0*pi/180) cos(0.0*pi/180)];
               robot_pose{ii}(1:3,(k-1)*4+4) = [0 0 corners_pose{ii*2}(3, (k-1)*4+4)]';
            end
        case 2
            robot_pose{ii} = repmat(eye(4,4), 1, scan_num);           
            for k = 1:scan_num
               robot_pose{ii}(1:2,(k-1)*4+1:(k-1)*4+2)  = [cos(90.0*pi/180) -sin(90.0*pi/180); sin(90.0*pi/180) cos(90.0*pi/180)];
               robot_pose{ii}(1:3,(k-1)*4+4) = [2.40 0 corners_pose{ii*2}(3, (k-1)*4+4)]';
               %robot_pose{ii}(1:3,(k-1)*4+4) = [2.46 0 corners_pose{ii*2}(3, (k-1)*4+4)]';
            end
        case 3
            robot_pose{ii} = repmat(eye(4,4), 1, scan_num);           
            for k = 1:scan_num
               robot_pose{ii}(1:2,(k-1)*4+1:(k-1)*4+2)  = [cos(180.0*pi/180) -sin(180.0*pi/180); sin(180.0*pi/180) cos(180.0*pi/180)];
               %robot_pose{ii}(1:3,(k-1)*4+4) = [2.46 12.225 corners_pose{ii*2}(3, (k-1)*4+4)]';
               robot_pose{ii}(1:3,(k-1)*4+4) = [2.4 12 corners_pose{ii*2}(3, (k-1)*4+4)]';
            end        
    end
end

%% 로봇?�서 ?�이???�서�?측정??코너�?contraints�??�정??
constraints_R2C = corners_pose;
for ii = 1:measurementID
    for k = 1:scan_num
        robot_pose_k = robot_pose{ceil(ii/2)}(:,(k-1)*4+1:(k-1)*4+4);
        corner_pose_k = corners_pose{ceil(ii)}(:,(k-1)*4+1:(k-1)*4+4);
        constraints_R2C{ii}(:,(k-1)*4+1:(k-1)*4+4) = (robot_pose_k^-1) * corner_pose_k;
    end  
end

%% 같�? 빔에??측정??코너??�?빔을 ??��?�는 로봇 ?�이??contraints�??�의 (dx,dy=0 ?�로 초기�?��)
constraints_C2R = cell(1,3);
for ii = 1:measurementID/2   
    constraints_C2R{ii} = repmat(eye(4,4), 1, scan_num);
    for k = 1:scan_num
        robot_pose_k = robot_pose{ceil(ii)}(:,(k-1)*4+1:(k-1)*4+4);
        corner_pose_k = eye(4,4);
        switch ii
            case 1
                corner_pose_k = corners_pose{5}(:,(k-1)*4+1:(k-1)*4+4); 
            case 2
                corner_pose_k = corners_pose{2}(:,(k-1)*4+1:(k-1)*4+4); 
            case 3
                corner_pose_k = corners_pose{4}(:,(k-1)*4+1:(k-1)*4+4); 
        end 
        constraints_C2R{ii}(:,(k-1)*4+1:(k-1)*4+4) = corner_pose_k^-1 * robot_pose_k;
        constraints_C2R{ii}(1:2, (k-1)*4+4) = zeros(2,1);
    end    
end


%% 같�? 빔에??측정??코너???�이??contraints�??�의 (dx,dy=0 ?�로 초기�?��)
constraints_C2C = cell(1,3);
for ii = 1:3 
    constraints_C2C{ii} = repmat(eye(4,4), 1, scan_num);
    for k = 1:scan_num
        refer_corner_pose_k = eye(4,4);
        target_corner_pose_k = eye(4,4);

        switch ii
            case 1
                refer_corner_pose_k = corners_pose{1}(:,(k-1)*4+1:(k-1)*4+4);
                target_corner_pose_k = corners_pose{3}(:,(k-1)*4+1:(k-1)*4+4); 
            case 2
                refer_corner_pose_k = corners_pose{3}(:,(k-1)*4+1:(k-1)*4+4);
                target_corner_pose_k = corners_pose{6}(:,(k-1)*4+1:(k-1)*4+4);              
            case 3
                refer_corner_pose_k = corners_pose{6}(:,(k-1)*4+1:(k-1)*4+4);
                target_corner_pose_k = corners_pose{1}(:,(k-1)*4+1:(k-1)*4+4); 
        end    
        constraints_C2C{ii}(:,(k-1)*4+1:(k-1)*4+4) = refer_corner_pose_k^-1 * target_corner_pose_k;
        constraints_C2C{ii}(1:2, (k-1)*4+4) = zeros(2,1);
    end    
end


%% 6�?Measurement?�서 ?�전??�?��??코너???�재 �?��??코너 ?�이??contraints�??�의 (dx,dy=0 ?�로 초기�?��)
constraints_C2C_at_6 = repmat(eye(4,4), 1, scan_num-1);
for k = 1:scan_num-1;
    prev_corner_pose_k = corners_pose{6}(:,(k-1)*4+1:(k-1)*4+4);
    curr_corner_pose_k = corners_pose{6}(:,k*4+1:k*4+4);
    
    constraints_C2C_at_6(:,(k-1)*4+1:(k-1)*4+4) = prev_corner_pose_k^-1 * curr_corner_pose_k;
    constraints_C2C_at_6(1:2, (k-1)*4+4) = zeros(2,1);
end



%% 그래??SLAM 초기??
import gtsam.*
isamParams = ISAM2Params;
isamParams.setFactorization('CHOLESKY');
isamParams.setRelinearizeSkip(100);
isamParams;
isam = gtsam.ISAM2(isamParams);
newFactors = NonlinearFactorGraph;
newValues = Values;

disp('-- Starting main loop: inference is performed at each time step, but we plot trajectory every 10 steps')

graph = NonlinearFactorGraph;
priorNoise = noiseModel.Diagonal.Precisions(0.0001* ones(6,1));


node_idx = 1;
graph.add(PriorFactorPose3(node_idx, Pose3(eye(4,4)), priorNoise));
newValues.insert(node_idx,Pose3(eye(4,4)));

global_odometry = eye(4,4);

%% 로봇 ?�드 ?�이??contraints�??�용??edge ?�성
robot_node_idx_set = zeros(measurementID/2,scan_num);
for ii = 1:measurementID/2
    %robot_pose_element = eye(4,4);
    %robot_pose_element(1:3,4) = robot_pose_mat(:,ii);
    for k = 1:scan_num
        robot_pose_element = robot_pose{ii}(:,(k-1)*4+1:(k-1)*4+4);
        
        node_idx = node_idx+1;
        newValues.insert(node_idx, Pose3(robot_pose_element)); 
        robot_node_idx_set(ii, k) = node_idx;

        curr_node_idx = robot_node_idx_set(ii, k);

        
        if(k == 1)
            graph.add(BetweenFactorPose3(1, curr_node_idx, Pose3(robot_pose_element), noise_model_for_init_robot_pose));   
        else     
            prev_node_idx = robot_node_idx_set(ii, k-1);
            
            robot_pose_element_prev = robot_pose{ii}(:,(k-2)*4+1:(k-2)*4+4);
            robot_pose_element_odom = robot_pose_element_prev^-1 * robot_pose_element;
            robot_pose_element_odom(1:2,4) = zeros(2,1);

            graph.add(BetweenFactorPose3(prev_node_idx, curr_node_idx, Pose3(robot_pose_element_odom), noise_model_for_step_robot_pose));   
        end      
    end
end

%% ?�이???�서�?측정??로봇�?코너 ?�이??contraints�??�용??edge ?�성
final_robot_node_idx = node_idx;
corner_node_idx_set = zeros(measurementID,scan_num);
for ii = 1:measurementID
    for k = 1:scan_num
        corner_pose = corners_pose{ii}(:,(k-1)*4+1:k*4);
        r2c_pose = constraints_R2C{ii}(:,(k-1)*4+1:k*4);
        
        node_idx =  node_idx+1;        
        newValues.insert(node_idx, Pose3(corner_pose));
        
        target_robot_node_idx = robot_node_idx_set(ceil(ii/2), k);
        graph.add(BetweenFactorPose3(target_robot_node_idx, node_idx, Pose3(r2c_pose), noise_model_for_robot_to_corner{ceil(ii/scan_num)}));   
        corner_node_idx_set(ii, k) = node_idx;
    end
end




%% 같�? 빔에??측정??코너???�당 빔에??측정???�행?�는 로봇?�이??contraints�??�용??edge ?�성
for ii = 1:measurementID/2 
    corner_type = 0;
    switch ii
        case 1
            corner_type = 5;
        case 2
            corner_type = 2;
        case 3
            corner_type = 4;
    end
    
    for k = 1:scan_num
        robot_idx = robot_node_idx_set(ii, k);
        corner_idx = corner_node_idx_set(corner_type, k);
        
        c2r_pose = constraints_C2R{ii}(:,(k-1)*4+1:k*4);
        graph.add(BetweenFactorPose3(corner_idx, robot_idx, Pose3(c2r_pose), noise_model_for_corner_to_robot{ii}));      
    end
end

%% 같�? 빔에??측정??코너???�이??contraints�??�용??edge ?�성
for ii = 1:3
    corner_type_to = 0;
    corner_type_from = 0;
    switch ii
        case 1           
            corner_type_from = 1;
            corner_type_to = 3;
        case 2            
            corner_type_from = 3;
            corner_type_to = 6;
        case 3
            corner_type_from = 6;
            corner_type_to = 1;            
    end
    for k = 1:scan_num
        corner_from_node_idx = corner_node_idx_set(corner_type_from, k);        
        corner_to_node_idx = corner_node_idx_set(corner_type_to, k);
        c2c_pose = constraints_C2C{ii}(:,(k-1)*4+1:k*4);
        graph.add(BetweenFactorPose3(corner_from_node_idx, corner_to_node_idx, Pose3(c2c_pose), noise_model_for_corner_to_corner));              
    end
end

%% 6�?Measurement?�서 ?�전??�?��??코너???�재 �?��??코너 ?�이??contraints�??�용??edge ?�성
for k = 1:scan_num-1
    curr_corner_to_node_idx = corner_node_idx_set(6, k+1);
    prev_corner_to_node_idx = corner_node_idx_set(6, k);
    
    c2c6_pose = constraints_C2C_at_6(:,(k-1)*4+1:k*4);
    graph.add(BetweenFactorPose3(prev_corner_to_node_idx, curr_corner_to_node_idx, Pose3(c2c6_pose), noise_model_for_step_corner));
end

pose_cell_guide_4 = corners_pose{6}(:,(scan_num-1)*4+1:scan_num*4);
graph.add(BetweenFactorPose3(1, corner_node_idx_set(6, scan_num), Pose3(pose_cell_guide_4), noise_model_for_init_robot_pose));  



figure(1)
plot3DTrajectory(newValues, 'g-o');
axis equal;


%% 그래??SLAM???�용??최적???�행
tic
isam.update(graph, newValues);
result = isam.calculateBestEstimate();
toc

figure(2)
plot3DTrajectory(result, 'g-o');
axis equal;


%% 결과 비교 분석???�한 매트�?�� �?��
pure_corners = Values;
opti_corners = Values;

pure_corners_mat = zeros(3,measurementID*scan_num+measurementID/2*scan_num);
opti_corners_mat = zeros(3,measurementID*scan_num+measurementID/2*scan_num);

for ii = 1:size(pure_corners_mat,2)
    pure_corners.insert(ii, newValues.at(ii+1).translation());
    opti_corners.insert(ii, result.at(ii+1).translation());
    pure_corners_mat(:,ii) = pure_corners.at(ii).vector();
    opti_corners_mat(:,ii) = opti_corners.at(ii).vector();
end

node_line1 = [opti_corners_mat(:, corner_node_idx_set(5,:)-1) opti_corners_mat(:, robot_node_idx_set(1,:)-1)];
node_line2 = [opti_corners_mat(:, corner_node_idx_set(2,:)-1) opti_corners_mat(:, robot_node_idx_set(2,:)-1)];
node_line3 = [opti_corners_mat(:, corner_node_idx_set(4,:)-1) opti_corners_mat(:, robot_node_idx_set(3,:)-1)];
node_line4 = [opti_corners_mat(:, corner_node_idx_set(1,:)-1) opti_corners_mat(:, corner_node_idx_set(3,:)-1) opti_corners_mat(:, corner_node_idx_set(6,:)-1)];


figure(10)
scatter3(node_line1(1,:), node_line1(2,:), node_line1(3,:));
axis equal;
[m, p, s] = best_fit_line(node_line1(1,:)',  node_line1(2,:)', node_line1(3,:)');
t = (20-m(3))/p(3);
max_point_line1 = [t*p(1)+m(1) t*p(2)+m(2) 20];
t = (0-m(3))/p(3);
min_point_line1 = [t*p(1)+m(1) t*p(2)+m(2) 0];
points_line1 = [max_point_line1; min_point_line1];
hold on;
plot3(points_line1(:,1), points_line1(:,2), points_line1(:,3));
incline1 = max_point_line1 - min_point_line1;
incline1_ang = atan(incline1(2)/incline1(3))*180/pi

figure(11)
scatter3(node_line2(1,:), node_line2(2,:), node_line2(3,:));
axis equal;
[m, p, s] = best_fit_line(node_line2(1,:)',  node_line2(2,:)', node_line2(3,:)');
t = (20-m(3))/p(3);
max_point_line2 = [t*p(1)+m(1) t*p(2)+m(2) 20];
t = (0-m(3))/p(3);
min_point_line2 = [t*p(1)+m(1) t*p(2)+m(2) 0];
points_line2 = [max_point_line2; min_point_line2];
hold on;
plot3(points_line2(:,1), points_line2(:,2), points_line2(:,3));
incline1 = max_point_line2 - min_point_line2;
incline2_ang = atan(incline1(2)/incline1(3))*180/pi

figure(12)
scatter3(node_line3(1,:), node_line3(2,:), node_line3(3,:));
axis equal;
[m, p, s] = best_fit_line(node_line3(1,:)',  node_line3(2,:)', node_line3(3,:)');
t = (20-m(3))/p(3);
max_point_line3 = [t*p(1)+m(1) t*p(2)+m(2) 20];
t = (0-m(3))/p(3);
min_point_line3 = [t*p(1)+m(1) t*p(2)+m(2) 0];
points_line3 = [max_point_line3; min_point_line3];
hold on;
plot3(points_line3(:,1), points_line3(:,2), points_line3(:,3));
incline1 = max_point_line3 - min_point_line3;
incline3_ang = atan(incline1(2)/incline1(3))*180/pi

figure(13)
scatter3(node_line4(1,:), node_line4(2,:), node_line4(3,:));
axis equal;
[m, p, s] = best_fit_line(node_line4(1,:)',  node_line4(2,:)', node_line4(3,:)');
t = (20-m(3))/p(3);
max_point_line4 = [t*p(1)+m(1) t*p(2)+m(2) 20];
t = (0-m(3))/p(3);
min_point_line4 = [t*p(1)+m(1) t*p(2)+m(2) 0];
points_line4 = [max_point_line4; min_point_line4];
hold on;
plot3(points_line4(:,1), points_line4(:,2), points_line4(:,3));
incline1 = max_point_line4 - min_point_line4;
incline4_ang = atan(incline1(2)/incline1(3))*180/pi


%% 최적??결과 비교 분석
figure(3);
plot3DPoints(pure_corners,'b.');
title('Odometry Result');
axis equal;
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

figure(4);
plot3DPoints(opti_corners,'r.');
title('iSAM Optimization Result');
axis equal;
xlabel('x(m)');
ylabel('y(m)');
zlabel('z(m)');

dlmwrite('pure_corners.txt',pure_corners_mat')
dlmwrite('opti_corners.txt',opti_corners_mat')


