#include <Eigen/Dense>
#include "mex.h"
#include "math.h"
#include <vector>
#include <cstdlib>
#include <ctime>

void validate_sizes(const mxArray* prhs[]) {
    if (mxGetNumberOfDimensions(prhs[0])!=2 || mxGetN(prhs[0])!=3) {
        mexErrMsgTxt("data's column number have to be 3");
    }
}

/* random permutation */
void randperm(int* randNumber, int N, int k) {
    int count=0;
    int* index = new int [N];
    
    for (int i=0; i<N; i++) index[i]=i;
    
    do {
        int idx = rand()%N;
        randNumber[count++]=index[idx];
        index[idx]=index[--N];
    } while(--k);
}

void mexFunction(int nlhs, mxArray* plhs[], const int nrhs, const mxArray* prhs[]) {
    
    if (nrhs!=5) {
        mexErrMsgTxt("Usage: data, maxIter, ec, ea, epsilon");
    }
    
    validate_sizes(prhs);
    // input 타입 체크
    double* data = (double*) mxGetData(prhs[0]);
    int numData = mxGetM(prhs[0]);
    int maxIter = mxGetPr(prhs[1])[0];
    double ec = mxGetPr(prhs[2])[0];
    double ea = mxGetPr(prhs[3])[0];
    double epsilon = mxGetPr(prhs[4])[0];
    
    // output 타입 정의
    mwSize ndim=2;
    mwSize dims[] = {1, 3};
    plhs[0] = mxCreateNumericArray(ndim, &dims[0], mxDOUBLE_CLASS, mxREAL);
    plhs[1] = mxCreateNumericArray(ndim, &dims[0], mxDOUBLE_CLASS, mxREAL);
    plhs[2] = mxCreateNumericArray(ndim, &dims[0], mxDOUBLE_CLASS, mxREAL);
    
    double* finalXc = (double*) mxGetData(plhs[0]);
    double* x1 = (double*) mxGetData(plhs[1]);
    double* x2 = (double*) mxGetData(plhs[2]);
    
    double dist[numData];
    double dist_best[numData]; 
    
    std::vector<std::size_t> inliers_line;
    std::vector<std::size_t> outliers_line;
    
    double dist1[numData];
    double dist2[numData];
    double emin=999.;
    
    // data를 N by 3 matrix로 변경.
    Eigen::MatrixXd mData = Eigen::Map<Eigen::MatrixXd>(data, numData, 3);
    
    /* Line Detection */
    double error_line;
    double min_error_line; 
    Eigen::Vector3d min_line; 
    
    int idx_in_line_b[numData];
    int idx_in_line[numData];
    int idx_out_line_b[numData];
    int idx_out_line[numData];
    
    int num_in_line_b=0;
    int num_in_line;
    int num_out_line_b=0;
    int num_out_line;
    
    
    
    int* randNumber = new int [2];

    int i;
    for (i=0;i<maxIter; i++) {
        
        randperm(randNumber, numData, 2);
        Eigen::Vector3d p1, p2;
      
        p1 = mData.row(randNumber[0]);
        p2 = mData.row(randNumber[1]);
        
        Eigen::Vector3d n = p1-p2;
        n = n/n.norm();
        Eigen::Vector3d a = p1; 
        error_line=0; 
        for (int j=0;j<numData; j++) {
            Eigen::Vector3d p = mData.row(j);
            Eigen::Vector3d ap = a-p; 
            Eigen::Vector3d apt = ap.transpose(); 
            Eigen::Vector3d dist_vector = ap-apt.dot(n)*n; 
            dist[j] = dist_vector.norm(); 
            error_line+=dist[j];
       }

        if (error_line < min_error_line || i==0) {
            min_error_line=error_line; 
            min_line = n;
            
            inliers_line.clear();
            outliers_line.clear(); 
            
            for(std::size_t j=0; j < numData; j++){
                if(dist[j] < 0.01){
                    inliers_line.push_back(j);
                }
                else outliers_line.push_back(j); 
            }
            
            if (inliers_line.size() >= 0.8*numData) break; 
        }
        

    }
 
//  inliers_line
//     mwSize ndim2=2;
//     mwSize dims2[] = {1, inliers_line.size()};
//     plhs[3] = mxCreateNumericArray(ndim, &dims[0], mxUINT8_CLASS, mxREAL);
//     unsigned int* idx_line1 = (unsigned int*) mxGetData(plhs[3]);
    
//     for (int i=0;i<inliers_line.size(); i++) {
//         idx_line1[i]=0; 
//     }
    
    mexPrintf("ITERATION FOR LINE: %d, %d/%d\n", i,inliers_line.size(),numData);
    
    /* Random Sampling for corner estimation */
    double cosAngle;
    Eigen::Vector3d xc, xa, xb, xc_xa, xc_xb;
    int *si = new int [2];
    
    for (int i=0;i<maxIter; i++) {
         for (int j=0;j<maxIter; j++) {
            
            randperm(si, inliers_line.size(), 2);
            
            xc = mData.row(inliers_line[si[0]]);
            xa = mData.row(inliers_line[si[1]]);
            xb = mData.row(outliers_line[si[0]%outliers_line.size()]);
            
             xc_xa = xc-xa;
             xc_xb = xc-xb;
            
             cosAngle = ((xc_xa).transpose()*(xc_xb));
             cosAngle/=xc_xa.norm();
             cosAngle/=xc_xb.norm();
             if(fabs((M_PI/2.0-acos(cosAngle))) < ea) break;
         }

        Eigen::MatrixXd mPc = xc.replicate(1, numData);
        Eigen::MatrixXd mPa = xa.replicate(1, numData);
        Eigen::MatrixXd mPb = xb.replicate(1, numData);
        
        Eigen::MatrixXd crossc = mData.transpose()-mPc;
        Eigen::MatrixXd crossa = mData.transpose()-mPa;
        Eigen::MatrixXd crossb = mData.transpose()-mPb;
        
        int numLine1=0, numLine2=0, numInlier=0;
        double error=0.0;
        
        for (int j=0; j<numData; j++) {
            Eigen::Vector3d vc = crossc.col(j);
            Eigen::Vector3d va = crossa.col(j);
            Eigen::Vector3d vb = crossb.col(j);
            
            Eigen::Vector3d v_cross_ca = vc.cross(va);
            Eigen::Vector3d v_cross_cb = vc.cross(vb);
            
            Eigen::Vector3d v_diff_ca = xc-xa;
            Eigen::Vector3d v_diff_cb = xc-xb;
            
            dist1[j]=sqrt(v_cross_ca.transpose()*v_cross_ca)/(v_diff_ca.norm()+0.000001);
            dist2[j]=sqrt(v_cross_cb.transpose()*v_cross_cb)/(v_diff_cb.norm()+0.000001);
            
            
            
            if (dist1[j]>dist2[j]) {
                error+=dist2[j];
            if (dist2[j]<ec) {
                numInlier++;
            }
             
//                 dist1[j]=dist2[j];
            }
            
//             error+=dist1[j];
        }
        
        error/=outliers_line.size();
        
//         if (i==0) {
//             for (int j=0;j<3;j++) {
//                 finalXc[j]=xc(j);
//                 x1[j] = xa(j);
//                 x2[j] = xb(j);
//             }
//         }

//         if (numInlier<numData*0.5 || numLine1==0 || numLine2==0) continue;
        
        // termination condition
        if (i==0 || error<emin) {
            emin = error;
            for (int j=0;j<3;j++) {
                finalXc[j]=xb(j);
                x1[j] = xa(j);
                x2[j] = xb(j);
            }
            
            if(numInlier > outliers_line.size()*0.8) break;
            //if (emin<epsilon) break;
            
        }
    }
    
}