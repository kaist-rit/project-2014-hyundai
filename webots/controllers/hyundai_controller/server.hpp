#ifndef	__SERVER_HPP__
#define __SERVER_HPP__

#include <iostream>
#include <cstdlib>
#include <boost/bind.hpp>
#include <boost/smart_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/timer/timer.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>

#include <iterator>
#include <list>
#include <string>


namespace goodguy{

    const std::size_t BUFFER_LENGTH = 1024;
    const std::size_t MAX_CONNECTIONS = 1024;


    using boost::asio::ip::tcp;

    
    // Session Class to communicate between webots and main controllers
    class session{

    public:
        // Constructor
        session(boost::asio::io_service& io_service, session** target)
            : target_(target), is_sending(false), socket(io_service), offset(0)
        {

        }

        // Destructor
        ~session(){

        }

        // Return socket of this session
        tcp::socket& getLocalSocket(){
            return socket;
        }

        // start async read operation
        void start(){
            socket.async_read_some(boost::asio::buffer(data_,BUFFER_LENGTH),
                boost::bind(&session::handle_read, this, 
                boost::asio::placeholders::error, 
                boost::asio::placeholders::bytes_transferred));
        }

        // define wrtie function to send packet to main controller
        void write(const std::vector<unsigned char>& src);
        void write(const std::string& src);


        // get received buffer to handle packet in webots controller
        std::vector<unsigned char> getReceivedBuffer(){
            std::vector<unsigned char> temp;
            mutex_for_read.lock();
            temp.insert(temp.begin(), received_buffer.begin(), received_buffer.end());
            received_buffer.clear();
            mutex_for_read.unlock();

            return temp;
        }

    private:

        // read handler from async read operation
        void handle_read(const boost::system::error_code& error, size_t bytes_transferred);
        
        // write handler from async write operation
        void handle_write(const boost::system::error_code& error);


    private:

        // mutex to prevent confliction between threads
        boost::mutex mutex_for_send;
        boost::mutex mutex_for_read;

        // session pointer to used in webots controller
        session** target_;

        bool is_sending;


        tcp::socket socket;

        int offset;

        unsigned char data_[BUFFER_LENGTH];
        unsigned char send_data[BUFFER_LENGTH];

        std::vector<unsigned char> received_buffer;
        std::vector<unsigned char> send_buffer;
    };



    // Server class 
    class server{

    public:
        // Constructor
        server(boost::asio::io_service& io_service, unsigned int port_num, session** target)
            : io_service_(io_service), target_(target), acceptor_(io_service, tcp::endpoint(tcp::v4(), port_num)), port_num_(port_num)
        {
            // Start accept connection request
            startAccept();
        }

    private:
        // start accept connection requeset
        void startAccept(){
            std::cout << "START SERVER, Port NUM: " << port_num_ << std::endl;
            // create new session to accept request
            session* new_session = new session(io_service_, target_);

            // start accept mode in async response
            acceptor_.async_accept(new_session->getLocalSocket(),
                boost::bind(&server::handleAccept, this, new_session, boost::asio::placeholders::error));
        }

        // handler is called when the connection request is arrived
        void handleAccept(session* new_session, const boost::system::error_code& error){
            if(!error){
                // start async read operation of session
                new_session->start();
                // register session to the webots controller session variable
                *target_ = new_session;
            }
            else{
                delete new_session;
            }

            // start another connection request for connection failure
            startAccept();
        }

    private:

        boost::asio::io_service& io_service_;
        session** target_;
        tcp::acceptor acceptor_;
        unsigned int port_num_;
    };

};

#endif
