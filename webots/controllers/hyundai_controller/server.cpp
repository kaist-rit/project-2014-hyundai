#include "server.hpp"

namespace goodguy{


    // send packet to main-controller 
    void session::write(const std::string& src){
        write(std::vector<unsigned char>(src.begin(), src.end()));

    }

    // send packet to main-controller 
    void session::write(const std::vector<unsigned char>& src){

        // save src into sendbuffer with mutex to prevent data confliction
        mutex_for_send.lock();
        send_buffer.insert(send_buffer.end(), src.begin(), src.end());
        mutex_for_send.unlock();
    
        // if not sending status then start it
        if(!is_sending){
            
            // mark it is sending status 
            is_sending = true;

            mutex_for_send.lock();
            std::size_t length = send_buffer.size();
            mutex_for_send.unlock();


            // if short data is remained
            if(((length - offset) < BUFFER_LENGTH) && ((length - offset) > 0)){

                mutex_for_send.lock();
                for( std::size_t i = 0; i < (length - offset); ++i){
                    send_data[i] = send_buffer[i+offset];
                }


                // async write operation
                boost::asio::async_write(socket, boost::asio::buffer(send_data,(length - offset)),
                        boost::bind(&session::handle_write, this, boost::asio::placeholders::error));

                offset += length - offset;	
                mutex_for_send.unlock();
            }
            // if long data is remained
            else if(((length - offset) >= BUFFER_LENGTH)){

                mutex_for_send.lock();
                for( std::size_t i = 0; i < BUFFER_LENGTH; ++i){
                    send_data[i] = send_buffer[i+offset];
                }


                // async write operation
                boost::asio::async_write(socket, boost::asio::buffer(send_data,BUFFER_LENGTH),
                        boost::bind(&session::handle_write, this, boost::asio::placeholders::error));

                offset += BUFFER_LENGTH;	
                mutex_for_send.unlock();
            }
            else{
                // if write operation is finished, then release sending status
                mutex_for_send.lock();
                send_buffer.clear();

                offset = 0;
                is_sending = false;
                mutex_for_send.unlock();
            }
        }


    }


    // async read handler
    void session::handle_read(const boost::system::error_code& error, size_t bytes_transferred){
        if(!error){

            mutex_for_read.lock();
            for(std::size_t i = 0; i < bytes_transferred; ++i){
                received_buffer.push_back(data_[i]);
            }
            mutex_for_read.unlock();

            socket.async_read_some(boost::asio::buffer(data_,BUFFER_LENGTH),
                    boost::bind(&session::handle_read, this, 
                        boost::asio::placeholders::error, 
                        boost::asio::placeholders::bytes_transferred));
        }
        else{
            *target_ = NULL;
            delete this;
        }

    }

    // async write handler
    void session::handle_write(const boost::system::error_code& error){
        if(!error){


            mutex_for_send.lock();
            std::size_t length = send_buffer.size();
            mutex_for_send.unlock();

            // if short data is remained
            if(((length - offset) < BUFFER_LENGTH) && ((length - offset) > 0)){

                mutex_for_send.lock();
                for( std::size_t i = 0; i < (length - offset); ++i){
                    send_data[i] = send_buffer[i+offset];
                }

                // async write operation
                boost::asio::async_write(socket, boost::asio::buffer(send_data,(length - offset)),
                        boost::bind(&session::handle_write, this, boost::asio::placeholders::error));

                offset += length - offset;	
                mutex_for_send.unlock();
            }
            // if long data is remained
            else if(((length - offset) >= BUFFER_LENGTH)){

                mutex_for_send.lock();
                for( std::size_t i = 0; i < BUFFER_LENGTH; ++i){
                    send_data[i] = send_buffer[i+offset];
                }

                // async write operation
                boost::asio::async_write(socket, boost::asio::buffer(send_data,BUFFER_LENGTH),
                        boost::bind(&session::handle_write, this, boost::asio::placeholders::error));

                offset += BUFFER_LENGTH;	
                mutex_for_send.unlock();
            }
            else{
                // if write operation is finished, then release sending status
                mutex_for_send.lock();
                send_buffer.clear();
                offset = 0;
                is_sending = false;
                mutex_for_send.unlock();
            }
        }
        else{

        }
    }

};

