#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/pwm.h"
#include "driverlib/qei.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "utils/uartstdio.h"

#include "packet.h"
#include "debug.h"


// Control frequency setting 
#define CONTROL_FREQ (200.0)

// PWM frequency setting 
#define PWM_FREQ (100*1000)

// Encoder resolution of motor
#define RESOLUTION 500 

// Gear ratio of motor
#define GEAR_RATIO 45 

#define BUFFER_LENGTH 256

char uart_recv_buffer[BUFFER_LENGTH]; 
char run_packet_buffer[BUFFER_LENGTH]; 

uint8_t uart_recv_size = 0;
uint8_t run_packet_idx = 0;

float run_time_diff = 1.0/CONTROL_FREQ;
float run_ideal_velocity = 0;


// Parsing Packet from Main Board (Odroid)
void parsingPacket(char* stream, uint8_t* stream_size);


// Get Direction from packet
int32_t getDirection(unsigned int selected_qei_base);

// Get Velocity from packet
float getVelocity(unsigned int selected_qei_base);

// Get Motor Position from packet
float getPosition(unsigned int selected_qei_base);

// Main Control function
void run(void);


// UART Interrupt Handler to communicate with Odroid
void UARTIntHandler(void);

// Timer Interrupt Handler to control motor
void Timer0IntHandler(void);

// Timer Configure fucntion
void TIMER0Configure();

// UART Configure fucntion
void UARTConfigure();

// LED Configure fucntion
void LEDConfigure();

// PWM Configure fucntion
void PWMConfigure();

// QEI Configure fucntion - to measure motor states (position, direction, velocity)
void QEIModuleConfigure();



// Send Velocity packet to Odroid board
void sendVelocity(float velocity){
    packet pk;

    // Generate Packet to send velocity to odroid
    generatePacketForSlaveToHostVel(velocity, &pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // Generate Packet stream(char array) 
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // Send packet to Odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

void sendAck(){
    packet pk;
    // Generate Packet to send ack to odroid
    generatePacketForAck(&pk);
    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // Generate Packet stream(char array) 
    generatePacketStream(&pk, packet_stream, &packet_stream_size);

    // Send packet to Odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

void packet_uart_echo_routine(packet* pk){

    char str[128];
    uint8_t str_size = 0;
    // Get echo message from odroid packet
    getEchoStringFromPacket(pk, str, &str_size);
    packet pk_send;
    // Generate Packet to send echo to odroid
    generatePacketForEchoTest(str, str_size, &pk_send);

    char packet_stream[128];
    uint8_t packet_stream_size = 0;

    // Generate Packet stream(char array) 
    generatePacketStream(&pk_send, packet_stream, &packet_stream_size);

    // Send packet to Odroid board
    UARTwrite(packet_stream, packet_stream_size);
}

void packet_initialize_routine(packet* pk){
    // Set ideal velocity to zero
    run_ideal_velocity = 0;
    sendAck();
}

void packet_host_to_slave_vel_routine(packet* pk){

    getVelocityFromPacket(pk, &run_ideal_velocity);
    sendAck();
}

void classifyPacket(packet* pk){
    // Classify packet into three-types 
    switch(pk->type){
        case PACKET_UART_ECHO:          packet_uart_echo_routine(pk);          break;
        case PACKET_INITIALIZE:         packet_initialize_routine(pk);         break;
        case PACKET_HOST_TO_SLAVE_VEL:  packet_host_to_slave_vel_routine(pk);  break;
        default: break;
    }
}

void parsingPacket(char* stream, uint8_t* stream_size){
    char header[4] = {0xff, 0xff, 0xff, 0x01};
    uint8_t header_size = 4;
    uint8_t result_size = 0;
    const char* remain_stream = NULL;
    uint8_t remain_stream_size = 0;

    // Split packet using the header char array
    const char* result = splitPacket(stream, header, *stream_size, header_size, &result_size);


    // Handling a first Splited char array
    if(result != NULL){
        packet packet_temp;

        // Get a Packet from char array
        if(getPacket(result, result_size, &packet_temp)){

            // Validity check
            if(isValid(&packet_temp)){
                // Handling packet as per type
                classifyPacket(&packet_temp);
            }
        }
        else{
            // remained packet
            remain_stream = result;
            remain_stream_size = result_size;
        }
    }
    else{
        // remained packet
        remain_stream = stream;
        remain_stream_size = *stream_size;
    }

    // Handling another Splited char array
    while((result = splitPacket(NULL, header, 0, header_size, &result_size)) != NULL){
        packet packet_temp;

        // Get a Packet from char array
        if(getPacket(result, result_size, &packet_temp)){

            // Validity check
            if(isValid(&packet_temp)){
                // Handling packet as per type
                classifyPacket(&packet_temp);
            }
        }
        else{
            // remained packet
            remain_stream = result;
            remain_stream_size = result_size;
        }
    }

    // if remained packet is exist (Not completely arrived packet)
    if(remain_stream != NULL){
        // Save remained packet
        *stream_size = remain_stream_size;
        for(int i = 0; i < remain_stream_size; ++i){
            stream[i] = remain_stream[i];
        }
    }
    else{
        *stream_size = 0;
    }
}


// Get Direction from QEI Module
int32_t getDirection(unsigned int selected_qei_base){
    int32_t dir = QEIDirectionGet(selected_qei_base);
    return dir;
}

// Get Velocity from QEI Module
float getVelocity(unsigned int selected_qei_base){
    float clock = SysCtlClockGet();
    float veldiv = 16;
    float speed = (float)QEIVelocityGet(selected_qei_base);

    float load = SysCtlClockGet()/4;
    float ppr = RESOLUTION*GEAR_RATIO;
    float edges = 4;

    int32_t dir = getDirection(selected_qei_base) * -1;
    float rpm = (clock * veldiv * speed * 60) * dir/(load * ppr * edges);
    return rpm;
}

// Get Position from QEI Module
float getPosition(unsigned int selected_qei_base){
    uint32_t val = QEIPositionGet(selected_qei_base);
    uint32_t max_position = RESOLUTION*4*GEAR_RATIO;

    float position; 
    position = -1.0*(float)val*360.0/(float)max_position;
    if (position>180) position-=360; 
    return position;
}


// PID Controller
void pid_controller(float cur_rpm, float cur_pos, float ideal_velocity){

    static float e[3];  // Error
    static float u[3];  // Control input

    // Save previous errors and control inputs
    for (int t=2; t>0; t--){
        e[t] = e[t-1];
        u[t] = u[t-1];
    }

    // PWM Control input
    int32_t control_input = 0;

    float Kp = 25.00;       // P GAIN
    float Kd = 3.0;         // D GAIN
    float Ki = 0.00;        // I GAIN

    // Calculate Velocity error between current and ideal
    e[0] = ideal_velocity - cur_rpm;

    static float integral = 0;
    integral += e[0] * run_time_diff;
    float derivative = (e[0] - e[1])/run_time_diff;

    // Calculate control input using PID gain
    u[0] = Kp*e[0] + Ki*integral + Kd*derivative;

    // Get PWM Frequency
    int32_t pwm_period = SysCtlClockGet()/(PWM_FREQ);

    static float accelation = 0;

    // Update Accelcation using control input
    accelation = u[0] + accelation;

    // Convert accelation into PWM Control input
    control_input = (int32_t)(accelation * run_time_diff + cur_rpm);

    // Prevent overflow boundaries of PWM period
    if(control_input < -350) control_input = -350;
    if(control_input > 350) control_input = 350;

    // Update PWM width using PWM Control input
    uint32_t pwm_width = (uint32_t)(400 + control_input);

    // Apply PWM width to QEI Module
    PWMPulseWidthSet(PWM0_BASE, PWM_OUT_5, pwm_width); 
    PWMSyncUpdate(PWM0_BASE,PWM_GEN_2_BIT);
}



void run(void){
    
    // Push back a stream array into buffer which is arrived from Odroid UART comm
    for(uint8_t i = 0; i < uart_recv_size; ++i){
        run_packet_buffer[run_packet_idx+i] = uart_recv_buffer[i];
    }

    run_packet_idx += uart_recv_size;
    uart_recv_size = 0;

    // Parsing Packet to handle packet 
    parsingPacket(run_packet_buffer, &run_packet_idx);

    // Get Motor velocity
    float rpm = getVelocity(QEI1_BASE);

    // Get Motor Position
    float pos = getPosition(QEI1_BASE);

    if(abs(run_ideal_velocity) >= 40.0){
        // if need maximum speed of robot during up-down motion
        uint32_t pwm_width = (uint32_t)(650);
        if(run_ideal_velocity > 0){
            PWMPulseWidthSet(PWM0_BASE, PWM_OUT_5, pwm_width);
        }
        else{
            PWMPulseWidthSet(PWM0_BASE, PWM_OUT_5, 800-pwm_width);
        }
        PWMSyncUpdate(PWM0_BASE,PWM_GEN_2_BIT);

    }
    else{
        // Conduct PID Control using current motor velocity
        pid_controller(rpm, pos, run_ideal_velocity);
    }

    // Send Velocity to Odroid Board
    sendVelocity(rpm);
}


// To test robot's up-down motion 
//   - first touch : UP
//   - second touch : neutral
//   - third touch : DOWN
//   - forth touch : neutral

void TouchSW1(void){
    static bool is_reverse = false;

    IntMasterDisable();
    uint32_t ui32Status;
    ui32Status = GPIOIntStatus(GPIO_PORTF_BASE, true);
    GPIOIntClear(GPIO_PORTF_BASE, ui32Status);

    if(run_ideal_velocity != 0){
        run_ideal_velocity = 0;
    }
    else if(is_reverse){
        run_ideal_velocity = -50;
        is_reverse = false;
    }
    else{
        run_ideal_velocity = 50;
        is_reverse = true;
    }
    IntMasterEnable();

}


// SW1 Switch Configuration to test robot's up-down motion
void SW1Configure(void){
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_4);
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_STRENGTH_12MA, GPIO_PIN_TYPE_STD_WPU);

    GPIOIntTypeSet(GPIO_PORTF_BASE, GPIO_PIN_4, GPIO_RISING_EDGE);
    GPIOIntRegister(GPIO_PORTF_BASE, TouchSW1);

    GPIOIntEnable(GPIO_PORTF_BASE, GPIO_INT_PIN_4);

    IntEnable(INT_GPIOF);

}

// Start-up Main function
int main(void){

    // Enable Floating-point unit
    FPUEnable();
    FPULazyStackingEnable();

    // Setting System clock to 80MHz
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    // LED Configuration
    LEDConfigure();


    // UART Configuration to communicate with Odroid Board throut RS-232
    UARTConfigure();
    UARTprintf("START MOTOR PWM PROGRAM\n");

    // QEI Module Configuration to measrue current motor state
    QEIModuleConfigure();

    // PWM Module Configuration to operate motor using PWM signal
    PWMConfigure();

    // Timer Configuration to control motor with precise period
    TIMER0Configure();

    // Switch Configuration to test robot's up-down motion
    SW1Configure();

    IntMasterEnable();

    while(1){
        //SysCtlDelay(SysCtlClockGet()/10);
    }
}


////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
//  HW DEPENDENT PART //////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
void UARTIntHandler(void){
    // If there are some characters, this function is called. 
    uint32_t ui32Status;
    // Get the interrrupt status.
    ui32Status = UARTIntStatus(UART0_BASE, true);

    // Clear the asserted interrupts.
    UARTIntClear(UART0_BASE, ui32Status);
    IntMasterDisable();
    // Loop while there are characters in the receive FIFO.

    while(UARTCharsAvail(UART0_BASE)){
        unsigned char input = UARTCharGetNonBlocking(UART0_BASE);

        if(uart_recv_size < BUFFER_LENGTH){
            uart_recv_buffer[uart_recv_size] = input;
            uart_recv_size = uart_recv_size + 1;
        }
    }
    IntMasterEnable();
}

void Timer0IntHandler(void){
    TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
    IntMasterDisable();

    // Execute run() function to control motor
    run();      
    IntMasterEnable();
}

void TIMER0Configure(){

    SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);

    TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);
    TimerLoadSet(TIMER0_BASE, TIMER_A, SysCtlClockGet()/CONTROL_FREQ);

    IntEnable(INT_TIMER0A);
    TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    TimerEnable(TIMER0_BASE, TIMER_A);

}

void UARTConfigure(){
    // Enable the peripherals used by this example.
    // GPIO PA[1:0]'s default state is UART0. 
    SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);

    // Set GPIO A0 and A1 as UART pins.
    GPIOPinConfigure(GPIO_PA0_U0RX);
    GPIOPinConfigure(GPIO_PA1_U0TX);
    GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    // Use the internal 16MHz oscillator as the UART clock source.
    UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);

    // Initialize the UART for console I/O.
    UARTStdioConfig(0, 1000000, 16000000);
    //UARTStdioConfig(0, 115200, 16000000);
    // Set baudrate for seral communication.

    // Enable the UART interrupt.
    IntEnable(INT_UART0);
    UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);

}


// LED Configuration 
void LEDConfigure(){

    // Enable the GPIO port that is used for the on-board LED.
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    // Enable the GPIO pins for the LED (PF2).
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
}


// PWM Configuration with PE5 pinout
void PWMConfigure(){

    uint32_t period = SysCtlClockGet()/(PWM_FREQ);

    // We will use GPIO E as PWM0. 
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIOPinConfigure(GPIO_PE5_M0PWM5);

    GPIOPinTypePWM(GPIO_PORTE_BASE, GPIO_PIN_5);

    PWMClockSet(PWM0_BASE, PWM_SYSCLK_DIV_1);

    // PWM Count-Down Mode 
    PWMGenConfigure(PWM0_BASE,PWM_GEN_2,PWM_GEN_MODE_DOWN|PWM_GEN_MODE_GEN_SYNC_GLOBAL);
    PWMGenPeriodSet(PWM0_BASE,PWM_GEN_2, period);
    PWMPulseWidthSet(PWM0_BASE,PWM_OUT_5, 1); 
    PWMGenEnable(PWM0_BASE,PWM_GEN_2);
    PWMOutputUpdateMode(PWM0_BASE,PWM_OUT_5,PWM_OUTPUT_MODE_SYNC_GLOBAL);
    PWMSyncUpdate(PWM0_BASE,PWM_GEN_2_BIT);
    PWMOutputState(PWM0_BASE,PWM_OUT_5_BIT,true);

}

// QEI Module Configuration to count encoder pulses with QEI1 (PC[5,6,4] pin)
void QEIModuleConfigure(){
    // Related to Encoder
    // Detect position and speed
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_QEI1);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);

    GPIOPinConfigure(GPIO_PC5_PHA1);
    GPIOPinConfigure(GPIO_PC6_PHB1);
    GPIOPinConfigure(GPIO_PC4_IDX1);

    GPIOPinConfigure(GPIO_PD6_PHA0);
    //GPIOPinConfigure(GPIO_PD7_PHB0);    // NOT WORKING!!
    GPIOPinConfigure(GPIO_PF1_PHB0);
    GPIOPinConfigure(GPIO_PF4_IDX0);
    
    GPIOPinTypeQEI(GPIO_PORTC_BASE, GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6);
    GPIOPinTypeQEI(GPIO_PORTD_BASE, GPIO_PIN_6 );
    GPIOPinTypeQEI(GPIO_PORTF_BASE, GPIO_PIN_4 | GPIO_PIN_1);

    uint32_t max_position = RESOLUTION*4*GEAR_RATIO;// max value counted in encoder. 
    uint32_t vel_period = SysCtlClockGet()/(4);

    // QEI_CONFIG_CAPTURE_A_B: Count on chA and chB edges
    // QEI_CONFIG_NO_RESET: Do not reset on index pulse
    // QEI_CONFIG_QUADRATURE: chA and chB are quadratures
    // QEI_CONFIG_NO_SWAP: Do not swap chA and chB
    QEIConfigure(QEI0_BASE, QEI_CONFIG_CAPTURE_A_B|QEI_CONFIG_NO_RESET|QEI_CONFIG_QUADRATURE|QEI_CONFIG_NO_SWAP, max_position);
    QEIConfigure(QEI1_BASE, QEI_CONFIG_CAPTURE_A_B|QEI_CONFIG_NO_RESET|QEI_CONFIG_QUADRATURE|QEI_CONFIG_NO_SWAP, max_position);
    QEIVelocityConfigure(QEI0_BASE, QEI_VELDIV_16, vel_period);
    QEIVelocityConfigure(QEI1_BASE, QEI_VELDIV_16, vel_period);

    QEIVelocityEnable(QEI0_BASE);
    QEIVelocityEnable(QEI1_BASE);

    QEIEnable(QEI0_BASE);
    QEIEnable(QEI1_BASE);

}
