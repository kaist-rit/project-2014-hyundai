#ifndef __CONTROLLER_HPP__
#define __CONTROLLER_HPP__

#include "packet.hpp"

#include <boost/asio.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>

#include <iostream>
#include <string>
#include <vector>


namespace goodguy{

    enum SID{
        LOWER,
        UPPER_1,
        UPPER_2
    };

    const int BUFFER_LENGTH = 128;

    template<class SOCKET>
        // controller class
        class Controller{

            public:
                // constructor
                Controller(SOCKET& socket, int control_time, SID sid)
                    : m_socket(socket), m_control_time(control_time), m_sid(sid)
                { }


                virtual void run(void) = 0;

                // start async read operation
                void start_read(){
                    m_mutex_for_comm.lock_upgrade();
                    m_socket.async_read_some(boost::asio::buffer(m_recv_buffer, BUFFER_LENGTH),
                            boost::bind(&Controller::handle_read, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                    m_mutex_for_comm.unlock_upgrade();
                }

                // send echo to control board
                void sendEcho(const std::string& str){
                    // make echo packet
                    PacketEcho<std::string> packet(m_sid, str);
                    std::vector<unsigned char> packet_stream = packet.generatePacketStream();
                    //write packet to control board
                    write(packet_stream);
                }

            private:
                

                // virtual received packet handler routines as per packet's type
                virtual void routineForEcho(const std::shared_ptr<PacketEcho<std::vector<unsigned char>>> ){ }
                virtual void routineForEchoTest(const std::shared_ptr<PacketEchoTest<std::vector<unsigned char>>> ){ }
                virtual void routineForInitialize(const std::shared_ptr<PacketInitialize> ){ }
                virtual void routineForHostToSlaveAng(const std::shared_ptr<PacketHostToSlaveAng> ){ }
                virtual void routineForSlaveToHostAng(const std::shared_ptr<PacketSlaveToHostAng> ){ }
                virtual void routineForHostToSlaveVel(const std::shared_ptr<PacketHostToSlaveVel> ){ }
                virtual void routineForHostToSlaveVel2(const std::shared_ptr<PacketHostToSlaveVel2> ){ }
                virtual void routineForSlaveToHostVel(const std::shared_ptr<PacketSlaveToHostVel> ){ }
                virtual void routineForSlaveToHostVel2(const std::shared_ptr<PacketSlaveToHostVel2> ){ }
                virtual void routineForAck(const std::shared_ptr<PacketAck> ){ }
                virtual void routineForSlaveComplete(const std::shared_ptr<PacketSlaveComplete> ){ }

            protected:

                // parsing packet from a received packet stream
                void parsePacket(void){

                    // split packet stream into sevaral streams using header
                    m_mutex_for_comm.lock_shared();
                    std::vector<std::string> splitted_packet = splitPacket(m_recv_packet_buffer);
                    m_mutex_for_comm.unlock_shared();

                    // clear buffer for receiving 
                    m_mutex_for_comm.lock_upgrade();
                    m_recv_packet_buffer.clear();
                    m_mutex_for_comm.unlock_upgrade();

                    for(auto s = splitted_packet.begin(); s != splitted_packet.end(); ++s){

                        try{
                            std::vector<unsigned char> packet_stream(s->begin(), s->end()); 
                            //printPacket(s->begin(), s->end());

                            // make packet from a packet stream
                            std::shared_ptr<Packet>  packet = parsePacketFromStream(packet_stream);

                            // go to packet handler as per packet's type
                            switch(packet->getPacketType()){
                                case PACKET_TYPE_ECHO: 
                                    {
                                        auto packet_echo = std::dynamic_pointer_cast<PacketEcho<std::vector<unsigned char>>>(packet);
                                        routineForEcho(packet_echo);
                                        break;
                                    }
                                case PACKET_TYPE_ECHO_TEST:
                                    {
                                        auto packet_echo_test = std::dynamic_pointer_cast<PacketEchoTest<std::vector<unsigned char>>>(packet);
                                        routineForEchoTest(packet_echo_test);
                                        break;
                                    }
                                case PACKET_TYPE_INITIALIZE:
                                    {
                                        auto packet_init = std::dynamic_pointer_cast<PacketInitialize>(packet);
                                        routineForInitialize(packet_init);
                                        break;
                                    }
                                case PACKET_TYPE_HOST_TO_SLAVE_ANG:
                                    {
                                        auto packet_ang = std::dynamic_pointer_cast<PacketHostToSlaveAng>(packet);
                                        routineForHostToSlaveAng(packet_ang);
                                        break;
                                    }
                                case PACKET_TYPE_SLAVE_TO_HOST_ANG:
                                    {
                                        auto packet_ang = std::dynamic_pointer_cast<PacketSlaveToHostAng>(packet);
                                        routineForSlaveToHostAng(packet_ang);
                                        break;
                                    }
                                case PACKET_TYPE_HOST_TO_SLAVE_VEL:
                                    {
                                        auto packet_vel = std::dynamic_pointer_cast<PacketHostToSlaveVel>(packet);
                                        routineForHostToSlaveVel(packet_vel);
                                        break;
                                    }
                                case PACKET_TYPE_HOST_TO_SLAVE_VEL2:
                                    {
                                        auto packet_vel = std::dynamic_pointer_cast<PacketHostToSlaveVel2>(packet);
                                        routineForHostToSlaveVel2(packet_vel);
                                        break;
                                    }
                                case PACKET_TYPE_SLAVE_TO_HOST_VEL:
                                    {
                                        auto packet_vel = std::dynamic_pointer_cast<PacketSlaveToHostVel>(packet);
                                        routineForSlaveToHostVel(packet_vel);
                                        break;
                                    }
                                case PACKET_TYPE_SLAVE_TO_HOST_VEL2:
                                    {
                                        auto packet_vel = std::dynamic_pointer_cast<PacketSlaveToHostVel2>(packet);
                                        routineForSlaveToHostVel2(packet_vel);
                                        break;
                                    }
                                case PACKET_TYPE_ACK:
                                    {
                                        auto packet_ack = std::dynamic_pointer_cast<PacketAck>(packet);
                                        routineForAck(packet_ack);
                                        break;
                                    }
                                case PACKET_TYPE_SLAVE_COMPLETE:
                                    {
                                        auto packet_slave_complete = std::dynamic_pointer_cast<PacketSlaveComplete>(packet);
                                        routineForSlaveComplete(packet_slave_complete);
                                        break;
                                    }

                                default: throw UnknownPacketException();
                            }

                        } catch (ShortPacketException& e){
                            if(s->size() != 0)
                            //std::cerr << "ShortPacketException: " << s->size() << std::endl;
                            if(s ==  splitted_packet.end()-1){
                                // if the remained packet stream is end of packet streams, then push back into buffer 
                                m_mutex_for_comm.lock_upgrade();
                                m_recv_packet_buffer.insert(m_recv_packet_buffer.begin(), s->begin(), s->end());
                                m_mutex_for_comm.unlock_upgrade();
                            }
                            continue;
                        } catch (ChecksumFailureException& e){
                            std::cerr << "ChecksumFailureException" << std::endl;
                            continue;
                        } catch (UnknownPacketException& e){
                            std::cerr << "UnknownPacketException" << std::endl;
                            continue;
                        } catch (InvalidDataException& e){
                            std::cerr << "InvalidDataException" << std::endl;
                            continue;
                        }

                    } 


                }

                // packet sync write to control board
                void write(const std::vector<unsigned char>& packet){
                    std::copy(packet.begin(), packet.end(), m_send_buffer);
                    if(packet.size() < BUFFER_LENGTH){
                        m_mutex_for_comm.lock_upgrade();
                        // sync write operation
                        m_socket.write_some(boost::asio::buffer(m_send_buffer, packet.size()));
                        m_mutex_for_comm.unlock_upgrade();
                    }
                    else{
                        std::cout << "ERROR, SEND BUFFER Overflow" << std::endl;
                    }
                }


                // read handler for async read operation
                void handle_read(const boost::system::error_code& error, size_t bytes_transferred){
                    // When receive packet through session
                    if(!error){
                        m_mutex_for_comm.lock_upgrade();
                        // copy received packet stream into buffer
                        m_recv_packet_buffer.insert(m_recv_packet_buffer.end(), std::begin(m_recv_buffer), std::begin(m_recv_buffer)+bytes_transferred);
                        // restart async read operation
                        m_socket.async_read_some(boost::asio::buffer(m_recv_buffer,BUFFER_LENGTH), 
                                boost::bind(&Controller::handle_read, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
                        m_mutex_for_comm.unlock_upgrade();
                    }
                    else{
                    }
                }

                // wait control time until reached target control time
                void waitControlTime(){

                    boost::timer::nanosecond_type diff = 0;

                    static boost::timer::auto_cpu_timer high_resolution_timer;
                    static boost::timer::nanosecond_type prior_time_count = high_resolution_timer.elapsed().wall;
                    static int accumulated_diff = 0;
                    static double control_time_mean = 0;
                    static double control_time_variance = 0;
                    static double control_time_count = 0;

                    const double scale = 0.5; // Scale == 0.3 일 때, Average = 10 ms

                    diff = (high_resolution_timer.elapsed().wall - prior_time_count);   // running에 필요한 시간을 고려하여 제어.


                    while( diff < (m_control_time-accumulated_diff*scale) ){

                        boost::timer::nanosecond_type wait_time = (m_control_time - accumulated_diff*scale - diff);

                        boost::this_thread::sleep(boost::posix_time::microseconds(wait_time/1000));
                        diff = high_resolution_timer.elapsed().wall - prior_time_count;
                    }

                    diff = high_resolution_timer.elapsed().wall - prior_time_count;
                    accumulated_diff += ((int)diff - m_control_time);

                    control_time_mean = (diff/1000.0/1000.0 + control_time_mean*control_time_count) / (control_time_count + 1);
                    control_time_variance = ((diff/1000.0/1000.0 - control_time_mean) * (diff/1000.0/1000.0 - control_time_mean)
                            + control_time_variance*control_time_count) / (control_time_count  + 1);
                    control_time_count = control_time_count + 1;

                    prior_time_count = high_resolution_timer.elapsed().wall;
                }


            protected: 
                SOCKET& m_socket;
                int m_control_time;
                SID m_sid;


                boost::shared_mutex m_mutex_for_comm;

                std::vector<unsigned char> m_recv_packet_buffer;

                unsigned char m_recv_buffer[BUFFER_LENGTH];
                unsigned char m_send_buffer[BUFFER_LENGTH];

        };

}

#endif 
