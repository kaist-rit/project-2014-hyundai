/*
 * packet.hpp
 *
 *  Created on: Aug 30, 2014
 *      Author: yonghoyoo and goodguy
 */

#ifndef __PACKET_HPP__
#define __PACKET_HPP__


#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>

#include <string>
#include <iterator>
#include <iostream>
#include <vector>
#include <numeric>

namespace goodguy {

    // Define packet type
    enum PacketType {
        PACKET_TYPE_ECHO                   =0, // echo 전송을 위한 packet
        PACKET_TYPE_ECHO_TEST              =1, // echo 전달을 위한 packet
        PACKET_TYPE_INITIALIZE             =2, // Motor 초기화, 두 모터의 angle을 0으로 설정
        PACKET_TYPE_HOST_TO_SLAVE_ANG      =3, // PC에서 제어보드로 각도를 보냄
        PACKET_TYPE_SLAVE_TO_HOST_ANG      =4, // 제어보드에서 PC로 각도를 보냄
        PACKET_TYPE_HOST_TO_SLAVE_VEL      =5, // PC에서 제어보드로 속도를 보냄
        PACKET_TYPE_HOST_TO_SLAVE_VEL2     =6, // PC에서 제어보드로 속도를 보냄
        PACKET_TYPE_SLAVE_TO_HOST_VEL      =7, // 제어보드에서 PC로 속도를 보냄
        PACKET_TYPE_SLAVE_TO_HOST_VEL2     =8, // 제어보드에서 PC로 속도를 보냄
        PACKET_TYPE_ACK                    =9, // 명령이 왔을 경우, 확인 메세지를 보냄
        PACKET_TYPE_SLAVE_COMPLETE         =10  // 제어보드에서 제어가 완료됨을 알림
    };


    struct ChecksumFailureException { };
    struct ShortPacketException { };
    struct WrongPacketException { };
    struct UnknownPacketException { };
    struct InvalidDataException { };


    /* Packet의 구성
     * header  (4byte): 0xff 0xff 0xff 0x01     				 (0~3)
     * length  (1byte): 전체 packet의 길이            			      (4)
     * sID     (1byte): 움직임을 제어할 sensor ID    				 (5)
     * type    (1byte): packet의 type             				 (6)
     * data    (4byte): 2byte는 상하모터 제어, 2byte는 좌우모터 제어  (7~10)
     * checksum(1byte): 전체 합이 0이 되어야 현재 packet이 유효       (11)
     */
    // Virtual PACKET class 
    class Packet {
        public:
            // constructor
            Packet(int sid, const PacketType& type) : m_sid(sid), m_type(type) { }

            // return packet type
            PacketType getPacketType() const noexcept { return m_type; }
            int getSID() const noexcept { return m_sid; }

            virtual std::vector<unsigned char> generatePacketStream() = 0;


        protected:
            // wrapper packet stream with header and tail
            std::vector<unsigned char> wrapPacketStream(const std::vector<unsigned char>& data = std::vector<unsigned char>()){
                std::vector<unsigned char> wrapped_packet_stream;
                unsigned char header[] = {0xff, 0xff, 0xff, 0x01};
                wrapped_packet_stream.insert(wrapped_packet_stream.end(), std::begin(header), std::end(header));
                wrapped_packet_stream.push_back((unsigned char)(4+1+1+1+data.size()+1));
                wrapped_packet_stream.push_back((unsigned char)m_sid);
                wrapped_packet_stream.push_back((unsigned char)m_type);
                wrapped_packet_stream.insert(wrapped_packet_stream.end(), std::begin(data), std::end(data));

                // Checksum to know transmittion failure
                unsigned char checksum = std::accumulate(wrapped_packet_stream.begin(), wrapped_packet_stream.end(), (unsigned char)0);
                wrapped_packet_stream.push_back(~checksum+1);
                return wrapped_packet_stream;
            }

            // to print defalut packet data struct
            friend void printDefault(std::ostream& os, Packet const& obj) {
                os << "SID: " << obj.m_sid << "\tTYPE: " ;
                switch(obj.m_type){
                    case PACKET_TYPE_ECHO               : os << "ECHO\t";            break;
                    case PACKET_TYPE_ECHO_TEST          : os << "ECHO TEST\t";       break;
                    case PACKET_TYPE_INITIALIZE         : os << "INITIALIZE\t";      break;
                    case PACKET_TYPE_HOST_TO_SLAVE_ANG  : os << "H2S ANGLE\t";       break;
                    case PACKET_TYPE_SLAVE_TO_HOST_ANG  : os << "S2H ANGLE\t";       break;
                    case PACKET_TYPE_HOST_TO_SLAVE_VEL  : os << "H2S VELOCITY\t";    break;
                    case PACKET_TYPE_HOST_TO_SLAVE_VEL2 : os << "H2S VELOCITY2\t";    break;
                    case PACKET_TYPE_SLAVE_TO_HOST_VEL  : os << "S2H VELOCITY\t";    break;
                    case PACKET_TYPE_SLAVE_TO_HOST_VEL2 : os << "S2H VELOCITY2\t";    break;
                    case PACKET_TYPE_ACK                : os << "ACK\t";             break;
                    case PACKET_TYPE_SLAVE_COMPLETE     : os << "SLAVE COMPLETE\t";  break;

                    default: throw UnknownPacketException();
                }
            }

        private:
            int m_sid;      	               // 현재 packet과 관련된 sensorID가 된다.
            PacketType m_type;   			   // 현재 packet의 type을 지정해준다.
    };

    // Packet for Echo
    template<class StringContainer>
    class PacketEcho : public Packet
    {
        public:
            // constructor
            PacketEcho(int sid, const StringContainer& data)
                : Packet(sid, PACKET_TYPE_ECHO), m_data(std::begin(data), std::end(data))
            { }

            // return echo message
            template<class ReturnContainer>
            ReturnContainer getString() const {
                return ReturnContainer(m_data.begin(), m_data.end());
            }

            // set echo message
            void setString(const StringContainer& str){
                m_data.clear();
                m_data.insert(m_data.begin(), std::begin(str), std::end(str));
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream(){
                return wrapPacketStream(m_data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketEcho const& obj) {
                printDefault(os, obj);
                os << "DATA: " << std::string(std::begin(obj.m_data), std::end(obj.m_data));
                return os;
            }

        private:
            std::vector<unsigned char> m_data;
    };

    // Packet for reply of echo
    template<class StringContainer>
    class PacketEchoTest : public Packet
    {
        public:
            // constructor
            PacketEchoTest(int sid, const StringContainer& data)
                : Packet(sid, PACKET_TYPE_ECHO_TEST), m_data(std::begin(data), std::end(data))
            { }

            // return echo reply message
            template<class ReturnContainer>
            ReturnContainer getString() const {
                return ReturnContainer(m_data.begin(), m_data.end());
            }

            // set echo reply message
            void setString(const StringContainer& str){
                m_data.clear();
                m_data.insert(m_data.begin(), std::begin(str), std::end(str));
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                return wrapPacketStream(m_data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketEchoTest const& obj) {
                printDefault(os, obj);
                os << "DATA: " << std::string(std::begin(obj.m_data), std::end(obj.m_data));
                return os;
            }
        private:
            std::vector<unsigned char> m_data;
    };

    // Packet for command of initialization 
    class PacketInitialize : public Packet
    {
        public:
            // constructor
            PacketInitialize(int sid)
                : Packet(sid, PACKET_TYPE_INITIALIZE)
            { }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                return wrapPacketStream();
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketInitialize const& obj) {
                printDefault(os, obj);
                return os;
            }
    };

    // Packet for command to move pan-tilt with target angles
    class PacketHostToSlaveAng : public Packet
    {
        public:
            // constructor
            PacketHostToSlaveAng(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_HOST_TO_SLAVE_ANG)
            {
                if(data.size() != sizeof(float)*2) throw InvalidDataException();
                m_motor1_ang = *((float*)&data[0]);
                m_motor2_ang = *((float*)&data[4]);
            }

            // constructor
            PacketHostToSlaveAng(int sid, float angle1, float angle2)
                : Packet(sid, PACKET_TYPE_HOST_TO_SLAVE_ANG), m_motor1_ang(angle1), m_motor2_ang(angle2) 
            {

            }

            // get pan angle
            float getMotor1Angle() const noexcept {
                return m_motor1_ang;
            }

            // get tilt angle
            float getMotor2Angle() const noexcept {
                return m_motor2_ang;
            }

            // set pan angle
            void setMotor1Angle(float angle){
                m_motor1_ang = angle;;
            }

            // set tilt angle
            void setMotor2Angle(float angle){
                m_motor2_ang = angle;;
            }

            // set pan & tilt angles
            void setMotorAngles(float motor1_ang, float motor2_ang){
                m_motor1_ang = motor1_ang;
                m_motor2_ang = motor2_ang;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(sizeof(float)*2);
                *((float*)&data[0]) = m_motor1_ang;
                *((float*)&data[4]) = m_motor2_ang;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketHostToSlaveAng const& obj) {
                printDefault(os, obj);
                os << "ANGLE1: " << obj.m_motor1_ang << "\tANGLE2: " << obj.m_motor2_ang;
                return os;
            }
        private:
            float m_motor1_ang;
            float m_motor2_ang;
    };

    // Packet for received command to move pan-tilt with target angles
    class PacketSlaveToHostAng : public Packet
    {
        public:
            // constructor
            PacketSlaveToHostAng(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_SLAVE_TO_HOST_ANG)
            {
                if(data.size() != sizeof(float)*2) throw InvalidDataException();
                m_motor1_ang = *((float*)&data[0]);
                m_motor2_ang = *((float*)&data[4]);
            }

            // constructor
            PacketSlaveToHostAng(int sid, float angle1, float angle2)
                : Packet(sid, PACKET_TYPE_SLAVE_TO_HOST_ANG), m_motor1_ang(angle1), m_motor2_ang(angle2) 
            {

            }

            // get pan angle
            float getMotor1Angle() const noexcept {
                return m_motor1_ang;
            }

            // get tilt angle
            float getMotor2Angle() const noexcept {
                return m_motor2_ang;
            }

            // set pan angle
            void setMotor1Angle(float angle){
                m_motor1_ang = angle;;
            }

            // set tilt angle
            void setMotor2Angle(float angle){
                m_motor2_ang = angle;;
            }

            // set pan & tilt angle
            void setMotorAngles(float motor1_ang, float motor2_ang){
                m_motor1_ang = motor1_ang;
                m_motor2_ang = motor2_ang;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(sizeof(float)*2);
                *((float*)&data[0]) = m_motor1_ang;
                *((float*)&data[4]) = m_motor2_ang;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketSlaveToHostAng const& obj) {
                printDefault(os, obj);
                os << "ANGLE1: " << obj.m_motor1_ang << "\tANGLE2: " << obj.m_motor2_ang;
                return os;
            }

        private:
            float m_motor1_ang;
            float m_motor2_ang;
    };

    // Packet for command to move up-down with target velocity
    class PacketHostToSlaveVel : public Packet
    {
        public:
            // constructor
            PacketHostToSlaveVel(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_HOST_TO_SLAVE_VEL)
            {
                if(data.size() != sizeof(float)*1) throw InvalidDataException();
                m_motor_vel = *((float*)&data[0]);
            }

            // constructor
            PacketHostToSlaveVel(int sid, float velocity)
                : Packet(sid, PACKET_TYPE_HOST_TO_SLAVE_VEL), m_motor_vel(velocity)
            { }

            // get up-down velocity
            float getMotorVelocity() const noexcept {
                return m_motor_vel;
            }

            // set up-down velocity
            void setMotorVelocity(float motor_vel){
                m_motor_vel = motor_vel;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(sizeof(float)*1);
                *((float*)&data[0]) = m_motor_vel;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketHostToSlaveVel const& obj) {
                printDefault(os, obj);
                os << "VELOCITY: " << obj.m_motor_vel;
                return os;
            }

        private:
            float m_motor_vel;
    };

    // Packet for command to move pan-tilt with target velocities
    class PacketHostToSlaveVel2 : public Packet
    {
        public:
            // constructor
            PacketHostToSlaveVel2(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_HOST_TO_SLAVE_VEL2)
            {
                if(data.size() != sizeof(float)*2) throw InvalidDataException();
                m_motor1_vel = *((float*)&data[0]);
                m_motor2_vel = *((float*)&data[sizeof(float)]);
            }

            // constructor
            PacketHostToSlaveVel2(int sid, float velocity1, float velocity2)
                : Packet(sid, PACKET_TYPE_HOST_TO_SLAVE_VEL2), m_motor1_vel(velocity1), m_motor2_vel(velocity2)
            { }

            // get pan velocity
            float getMotor1Velocity() const noexcept {
                return m_motor1_vel;
            }

            // get tilt velocity
            float getMotor2Velocity() const noexcept {
                return m_motor2_vel;
            }

            // set pan velocity
            void setMotor1Velocity(float motor_vel){
                m_motor1_vel = motor_vel;
            }

            // set tilt velocity
            void setMotor2Velocity(float motor_vel){
                m_motor2_vel = motor_vel;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(sizeof(float)*2);
                *((float*)&data[0]) = m_motor1_vel;
                *((float*)&data[sizeof(float)]) = m_motor2_vel;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketHostToSlaveVel2 const& obj) {
                printDefault(os, obj);
                os << "VELOCITY: " << obj.m_motor1_vel << ", " << obj.m_motor2_vel;
                return os;
            }

        private:
            float m_motor1_vel;
            float m_motor2_vel;
    };

    // Packet for received command to move up-down with target velocity
    class PacketSlaveToHostVel : public Packet
    {
        public:
            // constructor
            PacketSlaveToHostVel(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_SLAVE_TO_HOST_VEL)
            {
                if(data.size() != sizeof(float)*1) throw InvalidDataException();
                m_motor_vel = *((float*)&data[0]);
            }

            // constructor
            PacketSlaveToHostVel(int sid, float velocity)
                : Packet(sid, PACKET_TYPE_SLAVE_TO_HOST_VEL), m_motor_vel(velocity)
            { }

            // get up-down motion velocity
            float getMotorVelocity() const noexcept {
                return m_motor_vel;
            }

            // set up-down motion velocity
            void setMotorVelocity(float motor_vel){
                m_motor_vel = motor_vel;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(sizeof(float)*1);
                *((float*)&data[0]) = m_motor_vel;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketSlaveToHostVel const& obj) {
                printDefault(os, obj);
                os << "VELOCITY: " << obj.m_motor_vel;
                return os;
            }

        private:
            float m_motor_vel;
    };

    // Packet for received command to move pan-tilt with target velocities
    class PacketSlaveToHostVel2 : public Packet
    {
        public:
            // constructor
            PacketSlaveToHostVel2(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_SLAVE_TO_HOST_VEL2)
            {
                if(data.size() != sizeof(float)*2) throw InvalidDataException();
                m_motor1_vel = *((float*)&data[0]);
                m_motor2_vel = *((float*)&data[sizeof(float)]);
            }

            // constructor
            PacketSlaveToHostVel2(int sid, float velocity1, float velocity2)
                : Packet(sid, PACKET_TYPE_SLAVE_TO_HOST_VEL2), m_motor1_vel(velocity1), m_motor2_vel(velocity2)
            { }

            // get pan velocity
            float getMotor1Velocity() const noexcept {
                return m_motor1_vel;
            }

            // get tilt velocity
            float getMotor2Velocity() const noexcept {
                return m_motor2_vel;
            }

            // set pan velocity
            void setMotor1Velocity(float motor_vel){
                m_motor1_vel = motor_vel;
            }

            // set tilt velocity
            void setMotor2Velocity(float motor_vel){
                m_motor2_vel = motor_vel;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(sizeof(float)*2);
                *((float*)&data[0]) = m_motor1_vel;
                *((float*)&data[sizeof(float)]) = m_motor2_vel;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketSlaveToHostVel2 const& obj) {
                printDefault(os, obj);
                os << "VELOCITY: " << obj.m_motor1_vel << ", " << obj.m_motor2_vel;
                return os;
            }

        private:
            float m_motor1_vel;
            float m_motor2_vel;
    };


    // Packet for ACK notify that packet is received without any error
    class PacketAck : public Packet
    {
        public:
            // constructor
            PacketAck(int sid)
                : Packet(sid, PACKET_TYPE_ACK)
            { }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                return wrapPacketStream();
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketAck const& obj) {
                printDefault(os, obj);
                return os;
            }
    };

    // Packet for Slave completion notify that received task is done 
    class PacketSlaveComplete : public Packet
    {
        public:
            // constructor
            PacketSlaveComplete(int sid, const std::vector<unsigned char>& data)
                : Packet(sid, PACKET_TYPE_SLAVE_COMPLETE)
            {
                if(data.size() != sizeof(unsigned char)) throw InvalidDataException();
                m_complete_type = (PacketType)data[0];
            }

            // constructor
            PacketSlaveComplete(int sid, PacketType complete_type)
                : Packet(sid, PACKET_TYPE_SLAVE_COMPLETE), m_complete_type(complete_type) 
            { }
            
            // get what kind of command type is completed 
            PacketType getCompleteType() const noexcept {
                return m_complete_type;
            }

            // set what kind of command type is completed 
            void setCompleteType(PacketType complete_type){
                m_complete_type = complete_type;
            }

            // generate packet stream 
            std::vector<unsigned char> generatePacketStream() {
                std::vector<unsigned char> data(1);
                data[0] = (unsigned char)m_complete_type;
                return wrapPacketStream(data);
            }

            // print packet information
            friend std::ostream& operator<<(std::ostream& os, PacketSlaveComplete const& obj) {
                printDefault(os, obj);
                os << "Complete: " ;
                switch(obj.m_complete_type){
                    case PACKET_TYPE_ECHO               : os << "ECHO";            break;
                    case PACKET_TYPE_ECHO_TEST          : os << "ECHO TEST";       break;
                    case PACKET_TYPE_INITIALIZE         : os << "INITIALIZE";      break;
                    case PACKET_TYPE_HOST_TO_SLAVE_ANG  : os << "H2S ANGLE";       break;
                    case PACKET_TYPE_SLAVE_TO_HOST_ANG  : os << "S2H ANGLE";       break;
                    case PACKET_TYPE_HOST_TO_SLAVE_VEL  : os << "H2S VELOCITY";    break;
                    case PACKET_TYPE_HOST_TO_SLAVE_VEL2 : os << "H2S VELOCITY2";    break;
                    case PACKET_TYPE_SLAVE_TO_HOST_VEL  : os << "S2H VELOCITY";    break;
                    case PACKET_TYPE_SLAVE_TO_HOST_VEL2 : os << "S2H VELOCITY2";    break;
                    case PACKET_TYPE_ACK                : os << "ACK";             break;
                    case PACKET_TYPE_SLAVE_COMPLETE     : os << "SLAVE COMPLETE";  break;

                    default: throw UnknownPacketException();
                }
                return os;
            }

        private:
            PacketType m_complete_type;
    };

    // split packet stream using header
    std::vector<std::string> splitPacket(const std::vector<unsigned char>& packet_stream){

        std::vector<std::string> result;

        // header stream [0xff 0xff 0xff 0x01]
        std::string header;
        header.push_back((char)0xff);
        header.push_back((char)0xff);
        header.push_back((char)0xff);
        header.push_back((char)0x01);

        // split a packet stream into several streams
        boost::split_regex(result, packet_stream, boost::regex(header));

        if(result.size() > 0){
            for(std::size_t i = 1; i < result.size(); ++i){
                result[i].insert(result[i].begin(), header.begin(), header.end());
            }
        }


        return result;
    }

    // parse a packet stream into Packet class
    std::shared_ptr<Packet> parsePacketFromStream(const std::vector<unsigned char>& data) {

        // Occur short packet exception
        if(data.size() < 8) throw ShortPacketException();

        std::size_t length = data[4];

        // Occur short packet exception too
        if(data.size() != length) throw ShortPacketException();

        // sum(all packet)+check_sum=0이 되어야 한다.
        if(std::accumulate(data.begin(), data.end(), (unsigned char)0) != 0) {
            // Occur checksum failure exception 
            throw ChecksumFailureException();
        }

        // get board ID
        int sID  = data[5];

        // get Packet type
        PacketType type = (PacketType)data[6];

        std::vector<unsigned char> packet_data(data.begin()+7, data.end()-1);

        // Determine packet handler routine as per packet's type
        switch(type) {
            case PACKET_TYPE_ECHO               : return std::make_shared<PacketEcho<std::vector<unsigned char>>>(sID, packet_data);
            case PACKET_TYPE_ECHO_TEST          : return std::make_shared<PacketEchoTest<std::vector<unsigned char>>>(sID, packet_data);
            case PACKET_TYPE_INITIALIZE         : return std::make_shared<PacketInitialize>(sID);
            case PACKET_TYPE_HOST_TO_SLAVE_ANG  : return std::make_shared<PacketHostToSlaveAng>(sID, packet_data);
            case PACKET_TYPE_SLAVE_TO_HOST_ANG  : return std::make_shared<PacketSlaveToHostAng>(sID, packet_data);
            case PACKET_TYPE_HOST_TO_SLAVE_VEL  : return std::make_shared<PacketHostToSlaveVel>(sID, packet_data);
            case PACKET_TYPE_HOST_TO_SLAVE_VEL2 : return std::make_shared<PacketHostToSlaveVel2>(sID, packet_data);
            case PACKET_TYPE_SLAVE_TO_HOST_VEL  : return std::make_shared<PacketSlaveToHostVel>(sID, packet_data);
            case PACKET_TYPE_SLAVE_TO_HOST_VEL2 : return std::make_shared<PacketSlaveToHostVel2>(sID, packet_data);
            case PACKET_TYPE_ACK                : return std::make_shared<PacketAck>(sID);
            case PACKET_TYPE_SLAVE_COMPLETE     : return std::make_shared<PacketSlaveComplete>(sID, packet_data);

            default: throw UnknownPacketException();
        }


     }
    
    // print packet information
    template <class Iterator>
        void printPacket(const Iterator begin, const Iterator end){

            using value_type = typename std::iterator_traits<Iterator>::value_type;

            for(Iterator it = begin; it != end; ++it){

                value_type s = *it;
                unsigned char upper = (s >> 4) & 0xf;
                unsigned char lower = (s >> 0) & 0xf;
                const char ch[] = "0123456789ABCDEF";

                std::cout << ch[upper] << ch[lower] << " " ;
            }

            if(begin != end)
                std::cout << std::endl;
        }

}


#endif /* PACKET_HPP_ */
